/**
 * Created by Frederic on 19/03/2017.
 */

// FIRST, CHECK IF DIRECTORIES EXISTS AND IF NOT CREATE THEM SYNCHRONOUSLY!!
// MAKE A DIFFERENCE IF WIN32 OR OTHER (as the directory path is not the same)
var os = require('os');
var platform = undefined;
if (os.platform() === "WIN32") {
    platform = "windows";
} else {
    platform = "unix";
}
var imageDir = undefined;
if (platform === "windows") {
    imageDir = __dirname + "\\" + "images\\";
} else {
    imageDir = __dirname + "/images/";
}
var fs = require('fs');
if (!fs.existsSync(imageDir)) {
    console.log("Creating directory");
    fs.mkdirSync(imageDir, 0744);
    console.log("Directory created");
} else {
    console.log("Image directory already exists!");
}

var move = require('mv');
var restify = require('restify');
var server = restify.createServer();
var mongoose = require("mongoose");
var multipart = require("restify-multipart-body-parser");
var fire_admin = require("firebase-admin");

var serviceAccount = require("TakeABread-c0ee7e2470a4.json");

fire_admin.initializeApp({
	credential: fire_admin.credential.cert(serviceAccount),
	    databaseURL: "https://fir-takeabread.firebaseio.com"
  });

server.use(restify.jsonp());
server.use(restify.bodyParser());
server.use(multipart({
    multipart:true,
    limits:{
	fieldNameSize:10000,
	fieldSize:3096 * 3096
    }
}));


mongoose.connect("mongodb://127.0.0.1:27017/magasin", function(error, db) {
	//    if (error) return funcCallback(error);
    if (error) throw error;


    console.log("Connecté à la base de données 'magasin'");
});


mongoose.Promise = global.Promise;
// assert.equal(query.exec().constructor, global.Promise);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Yay we\'re connected!');
    // we're connected!
});

var adminSchema = mongoose.Schema({
	password:String,
	token:String
});
var admin = mongoose.model('admin', adminSchema);

var productSchema = mongoose.Schema({
    name:String,
    description:String,
    price:Number,
    promo_price:Number,
    availability:Boolean,
    promo:Boolean,
    imageUrl:String,
    imagePath:String
});

var product = mongoose.model('product', productSchema);



function createNewAdmin() {
    var tmpAdmin = new admin({password:'toto42sh'});
    if (findAdmin(tmpAdmin.password, null) === false) {
	console.log('TMP ADMIN : ' + JSON.stringify(tmpAdmin));
	tmpAdmin.save(function(err, result) {
		if (err) return console.error(err);
		console.log('SUPER !!');
	    });
    }
}

createNewAdmin();

function pingAdmin(req, res, next) {
    admin.findOne({password:"toto42sh"}, function(err, obj) {
	    if (err) return ;
	    // This registration token comes from the client FCM SDKs.
	    var registrationToken = obj.token;

	    // See the "Defining the message payload" section below for details
	    // on how to define a message payload.
	    var payload = {
        notification: {
          title: "Sa mere la pute",
          body: "jspr ça marche putain"
        },
        data: {
          dataTag:"dataValue",
          other:"Other"
        }
     };

	    // Send a message to the device corresponding to the provided
	    // registration token.
      console.log("RegistrationToken : " , registrationToken , " payload : " , JSON.stringify(payload));
	    fire_admin.messaging().sendToDevice(registrationToken, payload)
		.then(function(response) {
			// See the MessagingDevicesResponse reference documentation for
			// the contents of response.
			console.log("Successfully sent message:", JSON.stringify(response));
      res.send(200, {message:'Successfully pingd device'});
		    })
		.catch(function(error) {
			console.log("Error sending message:", JSON.stringify(error));
      res.send(500, {message:'error pinging device'});
		    });

	});

}

function updateAdmin(body, res, id) {
    var pass = body.password;
    var tok = body.token;
    console.log("Querying id : ", id, " password : " , pass);
    admin.findOneAndUpdate({ _id : id }, { token : tok }, {new:false,upsert:false, runValidators:true}, function(err, results) {
	    if (err) {
		res.json(
			 500,
			 {
			     status:500,
				 result:{
				 message:"Erreur updating the token"
				     }
			 }
			 );
	    }
	    else if (!err && results) {
		console.log("Find and modify : " , JSON.stringify(results));
		res.json(
			 200,
			 {
			     status:200,
				 result:{
				 token:'OK',
				     results: results
				     }
			 }
			 );
	    }
	    else {
		console.log("Dafuq ? " , JSON.stringify(err) , " | " , JSON.stringify(results));
		res.json(
			 500,
			 {
			     status:500,
				 result:{
				 message:"Error unknown"
				     }
			 }
			 );
	    }
	});
}

function findAdmin(body, res) {
    var pass = body.password;
    var token = body.token;
    if (pass === null || token === null) {
      res.json(
          500,
          {
              status:1,
              result:null,
              message:'Error getting result from database'
          }
      );
      next();
    }
    console.log("Gonna find if I got and admin with this password : " + JSON.stringify(pass));
    admin.find({
        password:pass
    }).
    exec(function(err, results) {
            if (err) {
                console.error("Got and error finding admin password");
		if (res === null) {
		    return false;
		}
                res.json(
                    500,
                    {
                        status:1,
                        result:null,
                        message:'Error getting result from database'
                    }
                );
            } else {
                if (results.length < 1) {
		    if (res === null) {
			return false;
		    }
		    res.json(
                        401,
                        {
                            status:401,
                            result:null,
                            message:"Wrong password"
                        }
                    );
		    //                    return;
                }
                console.log("Find admin got : ", results);
		if (res === null) {
		    return true;
		}
		console.log("Results : " , JSON.stringify(results));
		updateAdmin(body, res, results[0]._id);
//                 res.json(
//                     200,
//                     {
//                         status:200,
//                         result:{
//                             token:'OK'
//                         }
//                     }
//                 );
            }
    });
}

function pushProduct(newProduct, imagePath, res) {
    console.log("Coucou in pushProduct");
    newProductEntry = productSchema;
    console.log("[PUSHPRODUCT] DEBUG");
    newProductEntry.name = newProduct.name;
    console.log("[PUSHPRODUCT] DEBUG");
    newProductEntry.description = newProduct.description;
    console.log("[PUSHPRODUCT] DEBUG");
    newProductEntry.availability = newProduct.availability;
    console.log("[PUSHPRODUCT] DEBUG");
    newProductEntry.price = newProduct.price;
    console.log("[PUSHPRODUCT] DEBUG");
    newProductEntry.promo = newProduct.promo;
    console.log("[PUSHPRODUCT] DEBUG");
    newProductEntry.promo_price = newProduct.promo_price;
    console.log("[PUSHPRODUCT] DEBUG : " + imagePath);
    console.log("Typeof imagePath = " , typeof(imagePath));
    newProductEntry.imageUrl = imagePath.relative;
    newProductEntry.imagePath = imagePath.absolute;
    console.log("[PUSHPRODUCT] DEBUG");
    console.log("Pushing new product");
    var newProduct = new product(newProductEntry);
    newProduct.save(function(error, data) {
	if (error)  {
	    console.log("It fail miserably but don't know why");
	    res.json(error);
	} else {
	    console.log("Does it passed ? " + JSON.stringify(data));
	    res.json(200, data);
	}
    });
    console.log("Otherwise, I don't understand");
}

function requestCatalog(req, res, next) {
    console.log("In requestCatalog");
    product.find(null, function(err, pdts) {
	if (err) {res.json(500, err)};
	console.log("Catalog : " + JSON.stringify(pdts));
	if (pdts.length > 0) {
            console.log("Yeah catalog");
            console.log("Catalog = " + JSON.stringify(pdts));
            res.json(200, pdts);
	} else {
            res.json(500, {status:-1, message:'No entry in db', result:null});
	}
    });
//    catalog = db.products.find();
}


function login(req, res, next) {
    // Nothing at that time
    console.log("POST LOGIN : " + JSON.stringify(req.body));
    findAdmin(req.body, res);
    // Need to bind the admin to the device token to do notification
}

function getcatalog(req, res, next) {
    console.log("GET CATALOG");
    requestCatalog(req, res, next);
}

function putFileInImageDirectory(res, req) {
    console.log("Ok");
    console.log("Gonna move : " , JSON.stringify(req.files));

    var path_temp = req.files.imagePhoto.path;

    console.log("DEBUG");

    var currentFolder = process.cwd();

    console.log("DEBUG");

    var filename = 'filename.jpg';

    console.log("DEBUG");

    console.log("Moving image " + path_temp + " to " + imageDir + req.files.imagePhoto.name);

    move(path_temp, imageDir + req.files.imagePhoto.name, function(err) {
	console.log("debug");
	if (err) return console.error(err)

	var newPath = {
	    relative: req.files.imagePhoto.name,
	    absolute:imageDir+req.files.imagePhoto.name
	};
	console.log("file moved : " + newPath);

	pushProduct(req.body, newPath, res);
    });
}

function createProduct(req, res, next) {
    console.log("Creating a new product : " + JSON.stringify(req.body) + " image : " + JSON.stringify(req.files));
    putFileInImageDirectory(res, req);
    console.log("gros gros fuck");
}

function deliverImage(req, res, next) {
    console.log("Nye ?");
    console.log("Got an ask to deliver an image : " , req.params.id);
    var file = fs.readFile("./images/"+req.params.id, function(err, file){
	if (err) { res.send(500); return next(); }
	res.write(file);
	res.end();
	res.send({code:200, noEnd:true});
    });
}

function testRoute(req, res, next) {
    console.log("What I got : " , JSON.stringify(req.body) , " image : " , JSON.stringify(req.files));
}

function ping(req, res, next) {
    res.json(200, {status:'ok', message:'pong'});
}

server.get('/ping', ping);

server.post('/login', login);

server.get('/getcatalog', getcatalog);

server.post('/product/new', createProduct);

server.get('/images/:id', deliverImage);

server.get('/pingADM', pingAdmin);

server.listen(3030, function() {
    console.log('%s listen at %s', server.name, server.url);
});
